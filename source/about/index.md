---
title: Обо мне
type: about
---

## Обо мне

![Я](./me.jpg)

Я Евгений Гужихин, 24-летний frontend-разработчик из Санкт-Петербурга. На данный момент работаю в [Sravni.Ru](https://sravni.ru/).

## Использую в данный момент

- Компьютер: MacBook Pro 2020
- Хостинг: [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- IDE: [WebStorm](https://www.jetbrains.com/webstorm)
- Code Theme: [Darkula](https://draculatheme.com/jetbrains/)
- Браузер: [Safari](https://apple.com/)
- Терминал: [iTerm2](https://iterm2.com)

## Ссылки на меня

- Email: [gpont97@gmail.com](mailto:gpont97@gmail.com)
- LinkedIn: https://www.linkedin.com/in/евгений-гужихин-274021155/
- HH: https://spb.hh.ru/applicant/resumes/view?resume=2a05d008ff045a3b770039ed1f7a6b454e4956
- Github: https://github.com/gpont
- Telegram: https://t.me/gpont97
- Instagram: https://www.instagram.com/gpont31/
- StackOverflow: https://stackoverflow.com/users/4446359/
