const AVG_SPEED = 150;

/**
 * Average reading time calculation Helper
 * @description Generate page title.
 * @example
 *     <%- reading_time() %>
 */
hexo.extend.helper.register("reading_time", function (content) {
  const wordsCount = (content || this.page.content).split(' ').length;

  return `${Math.ceil(wordsCount / AVG_SPEED)} мин`;
});
