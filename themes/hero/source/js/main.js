function getScrollTop() {
  const doc = document.documentElement;
  return (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
}

document.addEventListener('DOMContentLoaded', function() {
  const nav = document.querySelector("#nav");
  const menuIcon = document.querySelector("#menu-icon");
  const themeSwitcher = document.querySelector("#theme-switcher");

  themeSwitcher.addEventListener('click', function() {
    themeSwitcher.classList.toggle('light');
    document.querySelector('body').classList.toggle('light-theme');
  });

  if (menuIcon === null) {
    return;
  }

  menuIcon.addEventListener('click', function() {
    nav.classList.toggle("hidden");
    menuIcon.classList.toggle("active");
  });

  window.addEventListener('scroll', function() {
    if (getScrollTop() > 50 && window.screen.width > 411) {
      nav.classList.add("hidden");
      menuIcon.classList.remove("active");
    } else {
      nav.classList.remove("hidden");
      menuIcon.classList.add("active");
    }
  });
});
